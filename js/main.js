const datos = {
    categories:[
        {
            text: "Personajes",
            url: "https://swapi.dev/api/people/",
        },
        {
            text: "Planetas",
            url: "https://swapi.dev/api/planets/"

        },
        {
            text: "Especies",
            url: "https://swapi.dev/api/species/",

        }
    ]
}

//Variable general para añadir

let main$$ = document.querySelector('main');

//Funcion crear enlace
function crearEnlace(linkText, funcionEnlace){
    const a$$=document.createElement('a');
    const link$$=document.createTextNode(linkText)
    a$$.appendChild(link$$);
    a$$.setAttribute('href', '#'); // '#'Para que permanezca siempre en la pagina

    a$$.addEventListener('click', function (e) { // la e es lo que se recoge de addevenlistener
        e.preventDefault();
        funcionEnlace();
        
    })
    return a$$;
}


//---------------CABECERA-----------------------//

function cabecera(){
    
    const cabecera$$ = document.createElement('div');
    main$$.appendChild(cabecera$$);
    cabecera$$.classList.add('header-img');
    //const imagenCabecera$$ = document.createElement('img');
    //imagenCabecera$$.src = '/assets/img/img-cabecera.png'; //Para asignar la imagen que queremos
    //cabecera$$.appendChild(imagenCabecera$$);

}

//--------------NAVEGADOR---------------------//

function navegador(){

    const nav$$ = document.createElement('div');
    main$$.appendChild(nav$$);
    nav$$.classList.add('nav-menu');

    for(let i=0; i<datos.categories.length;i++){
        console.log(datos.categories[i].text);
        const enlace$$=crearEnlace(datos.categories[i].text, function(){

            clearContainer(); //Creamos esta funcion para que cada vez que le demos a uno de los botones del enlace de personajes o vehiculos se limpie y se cargue al que hemos dado
            crearContenido(datos.categories[i].text); //crea el contenido de la categoria
            crearTabla(datos.categories[i].url); //crea el contenido de la categoria
        }
        );
        nav$$.appendChild(enlace$$);
        
    }

}
function clearContainer(){
    document.querySelector('article').innerHTML="";
}

//----------CONTENIDO-CUERPO---------------//

function contenido(){

        const contenido$$ = document.createElement('div');
        contenido$$.classList.add('cont');
        main$$.appendChild(contenido$$);
        const articulo$$ = document.createElement('article');
        contenido$$.appendChild(articulo$$);
        
}

function crearContenido(txt){
    
    const titulo$$ = document.createElement('h1');
    const tituloTxt$$ = document.createTextNode(txt);
    titulo$$.appendChild(tituloTxt$$);
    const articulo$$ = document.createElement('article');
    articulo$$.appendChild(titulo$$);
  }

//----------------CREAR TABLA--------------//

  function crearTabla(url){

    fetch(url).then
    (respuesta => respuesta.json()).then
    (function (data){

            console.log(data);
            console.log(data.results);
            const list$$ = document.createElement ('ul');
            const articulo$$ = document.querySelector('article');

            for (let i=0; i<=data.results.length; i++){
                const listItem$$ = document.createElement('li');
                
                const aTxt= crearEnlace(data.results[i].name, function() {
                    console.log(data.results[i]);
                    clearContainer();
                    crearContenido(data.results[i].name);
                    crearTabla(data.results[i]);
                });
                listItem$$.appendChild(aTxt);
                list$$.appendChild(listItem$$);
                articulo$$.appendChild(list$$);
        }
    }).catch( function (errors) {
        console.log(errors);
});
}


function crearContenidoSecundario(list) {
    const articulo = document.querySelector('article');
    const subTitulo = document.createElement('h1');
    const subTituloTxt = document.createTextNode(list);
    subTitulo.appendChild(subTituloTxt);
    articulo.appendChild(subTitulo);
  }
  
  function crearTablaSecundaria(list){
    const articulo = document.querySelector('article');
    const listDos = document.createElement('ul');
    listDos.classList.add('contenido');
  
    console.log(list);
  
    //for (i=1; i<=8;i++){
      for (let item in list){
      const txtTres= document.createTextNode(list[item]);
      const txtDos= document.createTextNode(item);
      const parDos= document.createElement('p');
      const parTres= document.createElement('p');
  
      parDos.appendChild(txtDos);
      parTres.appendChild(txtTres);
  
  
      const listItem = document.createElement('li');
      listItem.appendChild(txtDos);
      listItem.appendChild(parTres);
      listDos.appendChild(listItem);
    }
    articulo.appendChild(listDos);
  }





//Hay que ir añadiendo las funciones para que las cargue segun las vamos haciendo
window.onload = function(){
    cabecera();
    navegador();
    contenido();
   //window.onload cuando la pagina esta cargada llama a esta funcion
}