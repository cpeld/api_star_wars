const datos = {
   
    categories: [
      {
        text: "Personajes",
        path: "/personajes",
        url: "https://swapi.dev/api/people/",
      },
      {
        text: "Planetas",
        path: "/planetas",
        url: "https://swapi.dev/api/planets/",
      },
      {
        text: "Especies",
        path: "/especies",
        url: "https://swapi.dev/api/species/",
      },
    ],
  }
  
  
  /*------------CREA LOS ENLACES-------------*//*CAMBIADO POR BOTONES, ANTES ERAN <a>*/

  function crearEnlace(linkTxt, myCallback) {
      const btn$$ = document.createElement('buton');
      const link = document.createTextNode(linkTxt);
      btn$$.appendChild(link);
      btn$$.classList.add('btn-nav');
      //btn$$.setAttribute('href', '#');
  
      //const imagenNav = document.createElement('img');
      //imagenNav.src = './img/personajes.jpg';
      //a.appendChild(imagenNav);
  
      btn$$.addEventListener('click', function(e) {
          e.preventDefault();
          myCallback();
      });
      return btn$$;
  }
  

  /*--------------CABECERA----------------*/
  function cabecera(){
      const main = document.querySelector('main');
  
      const cabecera = document.createElement('div');
      cabecera.classList.add('header-img');
      main.appendChild(cabecera);
  }

  /*-------------NAV CON BOTONES-----------------*/
  function navegador(){
    const main = document.querySelector('main');
    const navegador = document.createElement('div');
    navegador.classList.add('nav-menu');
    main.appendChild(navegador);
  
    for (let i=0; i<=datos.categories.length-1; i++){
        const a = crearEnlace(datos.categories[i].text, function() {
            clearContainer();
            crearContenido(datos.categories[i].text);
            crearTabla(datos.categories[i].url);
        });
        navegador.appendChild(a)
        
    }
  }

  /*----------------CREA EL CONTENIDO H1 - DEL NAV -------------*/
  
  function crearContenido(txt){

    const articulo = document.querySelector('article');
    const titulo = document.createElement('h1');
    const tituloTxt = document.createTextNode(txt);
    titulo.appendChild(tituloTxt);
    articulo.appendChild(titulo);
    
  }
  
  /*------------LIMPIA BODY---------------*/
  
  function clearContainer() {
    document.querySelector('article').innerHTML = "";
  }
  

  /*--------------CREA CUERPO CONTENIDO--------*/
  function contenido(){
      const main = document.querySelector('main');
      const contenido = document.createElement('div');
      contenido.classList.add('div-cont-h1');
      main.appendChild(contenido);
      const articulo = document.createElement('article');
      contenido.appendChild(articulo);
  }

  /*--------------EXTRAER DATOS DE API-------------*/

  function crearTabla(url){
  
  fetch(url).then(response => response.json())
  
  
  .then(function (data) {
      console.log(data.results); /*----Resultados api

  /*--------------PRIMERA LISTA--------------*/
      const articulo = document.querySelector('article');
      const list = document.createElement('ul');
      list.classList.add('lista-ul-1');

              //DIV PARA LA IMAGEN
      const div$$ = document.createElement('div');
      div$$.classList.add('div-img');
      articulo.appendChild(div$$);

            //-------
      /*------------CREAR LI EN BASE A LOS RESULTADOS DE LA API BUSCADOS------------*/
      for (let i=0; i<=data.results.length; i++){
      const listItem = document.createElement('li');
      listItem.classList.add('li-1')
  
      /*------------CONST CREADA DE UNA FUNCION PARA RELLENAR LOS LI CON LOS DATOS DE LA API--------------*/
      const aTxt = crearEnlace(data.results[i].name, function() {
          clearContainer();
          crearContenidoSecundario(data.results[i].name);
          crearTablaSecundaria(data.results[i]);
        });
        /*-----------AÑADIR LOS DATOS EXTRAIDOS DE LA FUNCION ANTERIOR PARA USARLOS EN LA LISTA-----------*/
        listItem.appendChild(aTxt);
        list.appendChild(listItem);
        articulo.appendChild(list);
        }
        /*-----------CONTROL DE ERRORES-------------*/
      }).catch( function (errors) {
      console.log(errors);
      });
  }

/*-------------CREAR LISTA SECUNDARIA---------*/
  
  function crearContenidoSecundario(list) {
    const articulo = document.querySelector('article');
    const subTitulo = document.createElement('h1');
    subTitulo.classList.add('h1-sec'); //ESTO SERIA EL NOMBRE DEL PERSONAJE EN LA TABLA CON SUS CARACTERISTICAS
    const subTituloTxt = document.createTextNode(list);
    subTitulo.appendChild(subTituloTxt);
    articulo.appendChild(subTitulo);
  }

  /*-----------CREAR LOS ELEMENTOS DE LA LISTA SECUNDARIA--------*/
  function crearTablaSecundaria(list){
    const articulo = document.querySelector('article');
    const listDos = document.createElement('ul');
    listDos.classList.add('contenido-ul');
  
    console.log(list);


    /*-----FOR IN PARA CREAR LA LISTA SECUNDARIA--------*/
     for (let item in list){
      const txtTres= document.createTextNode(list[item]);
      const txtDos= document.createTextNode(item);
      const parDos= document.createElement('p');
      const parTres= document.createElement('p');
  
      parDos.appendChild(txtDos);
      parTres.appendChild(txtTres);
  
      const listItem = document.createElement('li');
      listItem.classList.add('li-2'); //Propiedades del nombre por ejemplo

      listItem.appendChild(txtDos);
      listItem.appendChild(parTres);
      listDos.appendChild(listItem);
    }

    articulo.appendChild(listDos);
  }
  
  //PARA QUE CARGUE LA PAGINA
  window.onload = function () {
      cabecera();  
      navegador();
      contenido();
    };